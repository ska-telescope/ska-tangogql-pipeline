## SKA Tangogql Pipeline

- SKA TangoGQL Pipeline is a project created to run all the specific tests and CI/CD requirements for the SKA project

- If you are looking for the actual code of tangoGQL please go to official repo https://gitlab.com/MaxIV/web-maxiv-tangogql 
 
- This project CI/CD is set to run every time a new push is done on the main repo, being on the main branch or any other branches

- Optionally a developer can trigger the pipeline manually and make use of the BRANCH var to set a specific branch for the pipeline to run

- To build images locally create a file called `PrivateRules.mak` with SECRET=*** 